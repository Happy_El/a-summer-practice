$(function() {

	// Custom JS

});

function switchTheme() {

	var elementHeader = document.getElementById("idHeader");
	elementHeader.classList.toggle('blue-theme');

	if (document.getElementById("idHeadBlock") != null){
	var elementHeadBlock = document.getElementById("idHeadBlock");
	elementHeadBlock.classList.toggle('blue-theme');
	}

	if (document.getElementById("idMainPage") != null){
	var elementMainPage = document.getElementById("idMainPage");
	elementMainPage.classList.toggle('blue-theme-main');
	}

	var elementBody = document.getElementById("idBody");
	elementBody.classList.toggle('blue-theme-body');

	var elementFooter = document.getElementById("idFooter");
	elementFooter.classList.toggle('blue-theme');

	if (document.getElementById("idInformation") != null){
	var elementInformation = document.getElementById("idInformation");
	elementInformation.classList.toggle('blue-theme-main');
	}
};
